import { Component, OnInit } from '@angular/core';
import {Product} from '../shared/models/Product';
import {ProductsService} from '../shared/services/products.service';
import {ShoppingCartService} from '../shared/services/shopping-cart.service';

@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.scss']
})
export class StoreComponent implements OnInit {
  products: Product[];
  count: number;
  total: number;
  search: string;

  constructor(private ps: ProductsService,
              private cs: ShoppingCartService) { }

  ngOnInit() {
    this.products = this.ps.getAllProducts();
    this.cs.loadItems();
    this.count = this.cs.getTotalCount();
    this.total = this.cs.getTotalPrice();
  }

  addItem(id) {
    const product = this.ps.items.find(item => item.id === id);
    this.cs.addItem(product.id, product.name, product.price, 1);
  }

}
