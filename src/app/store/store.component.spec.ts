import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StoreComponent } from './store.component';
import {Product} from '../shared/models/Product';
import {of} from 'rxjs';
import {FormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ProductsService} from '../shared/services/products.service';
import {NameFilterPipe} from '../shared/pipes/name-filter.pipe';
import {RouterTestingModule} from '@angular/router/testing';

fdescribe('StoreComponent', () => {
  let component: StoreComponent;
  let fixture: ComponentFixture<StoreComponent>;
  let productsTable;

  const products: Product[] = [
    {id: 1, name: 'ABC', description: 'DEF', price: 1, cal: 2,
      img: 'https://s3.us-east-2.amazonaws.com/xebec-test/products/APL.jpg',
      nutrients: null},
    {id: 2, name: 'HIJ', description: 'KLM', price: 3, cal: 4,
      img: 'https://s3.us-east-2.amazonaws.com/xebec-test/products/AVC.jpg',
      nutrients: null}
  ];

  let productsService = jasmine.createSpyObj('ProductsService', ['getAllProducts']);
  productsService.getAllProducts.and.returnValue(products);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        BrowserAnimationsModule,
        FormsModule,
        RouterTestingModule,
      ],
      declarations: [
        StoreComponent,
        NameFilterPipe
      ],
      providers: [
        {
          provide: ProductsService,
          useValue: productsService
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    productsTable = fixture.nativeElement.querySelector('.productList');
  });

 it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display products in table', async(() => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      expect(productsTable.children.length).toBe(4);
    });
  }));
});
