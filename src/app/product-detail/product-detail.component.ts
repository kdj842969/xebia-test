import {Component, DoCheck, OnInit} from '@angular/core';
import {Product} from '../shared/models/Product';
import {ActivatedRoute} from '@angular/router';
import {ProductsService} from '../shared/services/products.service';
import {ShoppingCartService} from '../shared/services/shopping-cart.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit, DoCheck {
  id: number;
  product: Product;
  count: number;
  price: number;
  caption: string[];
  range: string[];
  inCart: boolean;

  constructor(private ar: ActivatedRoute,
              private ps: ProductsService,
              private sps: ShoppingCartService) { }

  ngOnInit() {
    this.id = this.ar.snapshot.params.id;
    this.product = this.ps.items.find(item => item.id === +this.id);

    this.sps.loadItems();
    this.count = this.sps.getTotalCount();
    this.price = this.sps.getTotalPrice();
    this.caption = this.ps.caption;
    this.range = this.ps.range;
    this.inCart = this.sps.inShoppingCart(this.id);
  }

  ngDoCheck() {
    this.count = this.sps.getTotalCount();
    this.price = this.sps.getTotalPrice();
    this.inCart = this.sps.inShoppingCart(this.id);
  }

  addItem() {
    this.sps.addItem(this.id, this.product.name, this.product.price, 1);
    this.sps.loadItems();
  }

  removeItem() {
    this.sps.removeItem(this.id);
    this.sps.loadItems();
  }

}
