import { Pipe, PipeTransform } from '@angular/core';
import {Product} from '../models/Product';

@Pipe({
  name: 'nameFilter'
})
export class NameFilterPipe implements PipeTransform {

  transform(items: Product[], search: string): Product[] {
    if (!search) {
      return items;
    }
    search = search.toLowerCase();
    return items.filter(item => {
      return item.name.toLowerCase().includes(search);
    });
  }

}
