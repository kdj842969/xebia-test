import { Injectable } from '@angular/core';
import {Product} from '../models/Product';
import {CartItem} from '../models/CartItem';

@Injectable({
  providedIn: 'root'
})
export class ShoppingCartService {
  items: CartItem[] = [];
  constructor() {}

  loadItems() {
    let cartItems = localStorage && localStorage.getItem('cart') != null ? localStorage.getItem('cart') : null;
    if (cartItems && this.items && this.items.length <= 0) {
      try {
        cartItems = JSON.parse(cartItems);
        for (let i = 0; i < cartItems.length; i++) {
          const {id, name, price, quantity} = (Object)(cartItems[i]);
          if (id != null && name != null && price != null && quantity != null) {
            const item = new CartItem(+id, name, +price, +quantity);
            this.items.push(item);
          }
        }
      } catch (err) {
        console.error('Errors when reading local storage:', err);
      }
    }
  }

  saveItems() {
    if (localStorage != null) {
      localStorage.setItem('cart', JSON.stringify(this.items));
    }
  }

  addItem(id: number, name: string, price: number, quantity: number) {
    if (this.items.length <= 0) {
      const item = new CartItem(+id, name, +price, +quantity);
      this.items.push(item);
      console.log(this.items);
    } else {
      let found = false;
      for (let i = 0; i < this.items.length; i++) {
        const item = this.items[i];
        if (item.id === +id) {
          found = true;
          item.quantity += +quantity;
          if (item.quantity <= 0) {
            this.items.splice(i, 1);
          }
        }
      }

      if (!found) {
        const newItem = new CartItem(+id, name, +price, +quantity);
        this.items.push(newItem);
      }

    }
    this.saveItems();
  }

  getTotalPrice(): number {
    let total = 0;
    if (this.items) {
      for (let i = 0; i < this.items.length; i++) {
        const item = this.items[i];
        total += item.quantity * item.price;
      }
    }
    return total;
  }

  getTotalCount(): number {
    let count = 0;
    if (this.items) {
      for (let i = 0; i < this.items.length; i++) {
        count += 1;
      }
    }
    return count;
  }

  inShoppingCart(id: number): boolean {
    let flag = false;
    if (this.items) {
      for (let i = 0; i < this.items.length; i++) {
        const item = this.items[i];
        if (+item.id === id) {
          flag = true;
          break;
        }
      }
    }

    return flag;
  }

  removeItem(id) {
    if (this.items) {
      for (let i = 0; i < this.items.length; i++) {
        const item = this.items[i];
        if (+item.id === id) {
          this.items.splice(i, 1);
          break;
        }
      }
    }
    this.saveItems();
  }

  updateItems(id: number, quantity: number) {
    if (this.items) {
      for (let i = 0; i < this.items.length; i++) {
        const item = this.items[i];
        if (+item.id === id) {
          item.quantity = quantity;
          this.items.splice(i, 1, item);
          break;
        }
      }
    }
    this.saveItems();
  }

  clearCart() {
    this.items = [];
    this.saveItems();
  }
}
