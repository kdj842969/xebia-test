import { Injectable } from '@angular/core';
import {Product} from '../models/Product';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  items: Product[] = [
    {id: 1, name: 'Apple', description: 'Eat one every day to keep the doctor away!', price: 12, cal: 90,
      img: 'https://s3.us-east-2.amazonaws.com/xebec-test/products/APL.jpg',
      nutrients: {carotenoid: 0, vitamin: 2, folates: 0, potassium: 1, fiber: 2}},
    {id: 2, name: 'Avocado', description: 'Guacamole anyone?', price: 16, cal: 90,
      img: 'https://s3.us-east-2.amazonaws.com/xebec-test/products/AVC.jpg',
      nutrients: {carotenoid: 0, vitamin: 1, folates: 1, potassium: 1, fiber: 2}},
    {id: 3, name: 'Banana', description: 'These are rich in Potassium and easy to peel.', price: 4, cal: 120,
      img: 'https://s3.us-east-2.amazonaws.com/xebec-test/products/BAN.jpg',
      nutrients: {carotenoid: 0, vitamin: 2, folates: 1, potassium: 2, fiber: 2}},
    {id: 4, name: 'Cantaloupe', description: 'Delicious and refreshing.', price: 3, cal: 50,
      img: 'https://s3.us-east-2.amazonaws.com/xebec-test/products/CTP.jpg',
      nutrients: {carotenoid: 4, vitamin: 4, folates: 1, potassium: 2, fiber: 0}},
    {id: 5, name: 'Fig', description: 'OK, not that nutritious, but sooo good!', price: 10, cal: 100,
      img: 'https://s3.us-east-2.amazonaws.com/xebec-test/products/FIG.jpg',
      nutrients: {carotenoid: 0, vitamin: 0, folates: 0, potassium: 1, fiber: 2}},
    {id: 6, name: 'Grapefruit', description: 'Pink or red, always healthy and delicious.', price: 11, cal: 50,
      img: 'https://s3.us-east-2.amazonaws.com/xebec-test/products/GRF.jpg',
      nutrients: {carotenoid: 4, vitamin: 4, folates: 1, potassium: 1, fiber: 1}},
    {id: 7, name: 'Grape', description: 'Wine is great, but grapes are even better.', price: 8, cal: 100,
      img: 'https://s3.us-east-2.amazonaws.com/xebec-test/products/GRP.jpg',
      nutrients: {carotenoid: 0, vitamin: 3, folates: 0, potassium: 1, fiber: 1}},
    {id: 8, name: 'Guava', description: 'Exotic, fragrant, tasty!', price: 8, cal: 50,
      img: 'https://s3.us-east-2.amazonaws.com/xebec-test/products/GUA.jpg',
      nutrients: {carotenoid: 4, vitamin: 4, folates: 0, potassium: 1, fiber: 2}},
    {id: 9, name: 'Kiwi', description: 'These come from New Zealand.', price: 14, cal: 90,
      img: 'https://s3.us-east-2.amazonaws.com/xebec-test/products/KIW.jpg',
      nutrients: {carotenoid: 1, vitamin: 4, folates: 0, potassium: 2, fiber: 2}},
    {id: 10, name: 'Lychee', description: 'Unusual and highly addictive!', price: 18, cal: 125,
      img: 'https://s3.us-east-2.amazonaws.com/xebec-test/products/LYC.jpg',
      nutrients: {carotenoid: 1, vitamin: 4, folates: 0, potassium: 2, fiber: 2}},
    {id: 11, name: 'Mango', description: 'Messy to eat, but well worth it.', price: 8, cal: 70,
      img: 'https://s3.us-east-2.amazonaws.com/xebec-test/products/MAN.jpg',
      nutrients: {carotenoid: 3, vitamin: 4, folates: 0, potassium: 1, fiber: 1}},
    {id: 12, name: 'Orange', description: 'Vitamin C anyone? Go ahead, make some juice.', price: 9, cal: 70,
      img: 'https://s3.us-east-2.amazonaws.com/xebec-test/products/ORG.jpg',
      nutrients: {carotenoid: 1, vitamin: 4, folates: 2, potassium: 1, fiber: 2}},
    {id: 13, name: 'Papaya', description: 'Super-popular for breakfast.', price: 5, cal: 60,
      img: 'https://s3.us-east-2.amazonaws.com/xebec-test/products/PAP.jpg',
      nutrients: {carotenoid: 3, vitamin: 4, folates: 2, potassium: 2, fiber: 2}},
    {id: 14, name: 'Peach', description: 'Add some cream and enjoy.', price: 19, cal: 70,
      img: 'https://s3.us-east-2.amazonaws.com/xebec-test/products/PCH.jpg',
      nutrients: {carotenoid: 1, vitamin: 2, folates: 0, potassium: 1, fiber: 2}},
    {id: 15, name: 'Pear', description: 'Delicious fresh, or cooked in red wine, or distilled.', price: 8, cal: 100,
      img: 'https://s3.us-east-2.amazonaws.com/xebec-test/products/PER.jpg',
      nutrients: {carotenoid: 0, vitamin: 2, folates: 0, potassium: 1, fiber: 2}},
    {id: 16, name: 'Pomegranate', description: 'Delicious, healthy, beautiful, and sophisticated!', price: 19, cal: 110,
      img: 'https://s3.us-east-2.amazonaws.com/xebec-test/products/PMG.jpg',
      nutrients: {carotenoid: 0, vitamin: 2, folates: 0, potassium: 2, fiber: 2}},
    {id: 17, name: 'Pineapple', description: 'Enjoy it (but don\'t forget to peel first).', price: 4, cal: 60,
      img: 'https://s3.us-east-2.amazonaws.com/xebec-test/products/PNP.jpg',
      nutrients: {carotenoid: 0, vitamin: 3, folates: 0, potassium: 0, fiber: 1}},
    {id: 18, name: 'Persimmon', description: 'Believe it or not, they are berries!', price: 6, cal: 120,
      img: 'https://s3.us-east-2.amazonaws.com/xebec-test/products/PSM.jpg',
      nutrients: {carotenoid: 4, vitamin: 3, folates: 0, potassium: 1, fiber: 3}},
    {id: 19, name: 'Strawberry', description: 'Beautiful, healthy, and delicious.', price: 7, cal: 40,
      img: 'https://s3.us-east-2.amazonaws.com/xebec-test/products/STR.jpg',
      nutrients: {carotenoid: 0, vitamin: 4, folates: 1, potassium: 1, fiber: 2}},
    {id: 20, name: 'Tangerine', description: 'Easier to peel than oranges!', price: 8, cal: 50,
      img: 'https://s3.us-east-2.amazonaws.com/xebec-test/products/TNG.jpg',
      nutrients: {carotenoid: 3, vitamin: 4, folates: 1, potassium: 1, fiber: 2}},
    {id: 21, name: 'Watermelon', description: 'Nothing comes close on those hot summer days.', price: 4, cal: 90,
      img: 'https://s3.us-east-2.amazonaws.com/xebec-test/products/WML.jpg',
      nutrients: {carotenoid: 4, vitamin: 4, folates: 0, potassium: 1, fiber: 1}}
  ];

  caption: string[] = ['Negligible', 'Low', 'Average', 'Good', 'Great'];
  range: string[] = ['below 5%', 'between 5 and 10%', 'between 10 and 20%', 'between 20 and 40%', 'above 40%'];

  constructor() { }

  getAllProducts(): Product[] {
    return this.items;
  }
}
