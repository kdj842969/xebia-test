export class Product {
  id?: number;
  name: string;
  description: string;
  price: number;
  cal: number;
  img: string;
  nutrients: {
    carotenoid: number;
    vitamin: number;
    folates: number;
    potassium: number;
    fiber: number;
  };
}
