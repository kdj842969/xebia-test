export class CartItem {
  id: number;
  name: string;
  price: number;
  quantity: number;

  constructor(id, name, price, quantity) {
    this.id = id;
    this.name = name;
    this.price = price;
    this.quantity = quantity;
  }
}
