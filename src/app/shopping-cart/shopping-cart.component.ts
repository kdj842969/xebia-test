import {Component, DoCheck, OnInit} from '@angular/core';
import {ShoppingCartService} from '../shared/services/shopping-cart.service';
import {CartItem} from '../shared/models/CartItem';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.scss']
})
export class ShoppingCartComponent implements OnInit, DoCheck {
  totalCount: number;
  totalPrice: number;
  items: CartItem[];
  msg = '';

  constructor(private ps: ShoppingCartService) { }

  ngOnInit() {
    this.ps.loadItems();
    this.totalCount = this.ps.getTotalCount();
    this.totalPrice = this.ps.getTotalPrice();
    this.items = this.ps.items;
  }

  ngDoCheck() {
    this.totalCount = this.ps.getTotalCount();
    this.totalPrice = this.ps.getTotalPrice();
    this.items = this.ps.items;
  }

  saveItems(id: number, quantity: number) {
    this.ps.updateItems(id, quantity);
  }

  removeItem(id) {
    this.ps.removeItem(id);
    this.ps.loadItems();
  }

  addItem(id: number, name: string, price: number, quantity: number) {
    this.ps.addItem(id, name, price, quantity);
    this.ps.loadItems();
  }

  clearCart() {
    this.ps.clearCart();
    this.ps.loadItems();
  }
}
