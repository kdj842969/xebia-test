# XebiaTest - Grocery Store APP

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.2.1.

## Assumption

* Functionality
  * display all the products
  * show products details such as images, description, nutrients
  * display all the items in the shopping bag, change quantity, or delete items from shopping bag.

* Design decision
  * shopping bag is stored in browser local storage. Local storage offers a more simple intuitive interface to store data.
  * for products count, the same type of product will be considered as 1.
  * Every product has quantity limit in shopping cart from [1, 1000]. The add and remove button will be 
  
* Test case
  * There are two test cases for store component, implemented with Jasmine and Karma.

* Live demo
  * A live demo has been already deployed on AWS S3: [live demo here](http://xebec-test.s3-website.us-east-2.amazonaws.com)

## Run Guide

### Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

### Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

### Running lint
Run `ng lint` to check the best practice.